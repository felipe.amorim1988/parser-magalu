// errorHandler.js

function errorHandler(err, req, res, next) {
    // Definir o status de resposta para 500 (Internal Server Error)
    res.status(500).json({ error: err.message });
}

module.exports = errorHandler;
