const OrderService =require('../services/orderService')
class OrdersController{
    static async getAll(req,res,next){
        try {
            res.json(await OrderService.getOrders())
        } catch (error) {
            next(error)
        }
    }
    static async getById(req,res,next){
        try {
            res.json(await OrderService.getOrderByOrderId(req.params.orderId))
        } catch (error) {
            next(error)
        }
    }
    static async getByDate(req,res,next){
        try {
            res.json(await OrderService.getOrdersByDate(req.params.startDate,req.params.endDate))
        } catch (error) {
            next(error)
        }
    }
}
module.exports = OrdersController;
