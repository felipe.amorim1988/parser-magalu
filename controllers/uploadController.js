const UploadService = require('../services/uploadService');
const fs = require('fs');


class UploadController {
    static async uploadFile(req, res,next) {
        try {
            if (!req.file) {
                return res.status(400).json({ error: 'Nenhum arquivo enviado' });
            }
    
            const filePath = req.file.path;
            const objects = await UploadService.processFile(filePath);
    
            fs.unlinkSync(filePath); // Deleta o arquivo após a leitura
    
            res.json(objects);
        } catch (error) {
            next(error)
        }
    }
}

module.exports = UploadController;
