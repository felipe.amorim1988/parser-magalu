# parser-magalu

Teste técnico em nodejs utilizando express, ORM Sequelize, mensageria com rabbitmq e dados em base MySQL


## Instalação

1. Clone o repositório:

2. Instale as dependências:

3. Configure as variáveis de ambiente:

Crie um arquivo `.env` na raiz do projeto e adicione as variáveis de acordo com o arquivo `.env-sample`

4. Existem duas formas de construir o ambiente:
- Utilizando "docker-compose up -d" na raiz do projeto para criar as instancias de container do banco, do rabbitmq, da api e do worker
- Caso ja tenha alguma conexao rabbit e mysql, configurar no .env e executar "npm start" e "npm worker"

5. O fluxo principal consiste em:
- um endpoint POST http://localhost:3000/upload (formdata) contendo o arquivo txt para importação, o mesmo converte as linhas em um json que é enviado para uma fila rabbitmq
- O worker.js lê a lista no json da fila e processa item a item inserindo os registros que ainda não existem em suas respectivas tabelas
- São espostos 3 endpoints para consulta dos dados importados:
    - http://localhost:3000/orders (lista todos os pedidos importados)
    - http://localhost:3000/orders/:orderId (lista pedido pelo orderId e os produtos contidos nele)
    - http://localhost:3000/orders/:startDate/:endDate (lista pedidos em um intervalo de tempo)

