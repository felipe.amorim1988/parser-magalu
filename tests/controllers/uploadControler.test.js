const UploadController = require('../../controllers/uploadController');
const UploadService = require('../../services/uploadService');

const processedData = [
    {
        userId: "0000000070",
        userName: "Palmer Prosacco",
        orders: [
            {
                orderId: "0000000753",
                date: "20210308",
                products: [
                    {
                        prodId: "0000000003",
                        value: 1836.74
                    },
                    {
                        prodId: "0000000004",
                        value: 618.79
                    }
                ]
            },
        ]
    }
];
const req = { file: { path: 'tests/test.txt' } };
const res = {
    status: jest.fn(() => res),
    json: jest.fn()
};
const next = jest.fn();

// Mock de amqplib
jest.mock('amqplib', () => {
    const mockSendToQueue = jest.fn();
    const mockClose = jest.fn();
    const mockChannel = {
        assertQueue: jest.fn(),
        sendToQueue: mockSendToQueue,
        close: mockClose
    };
    const mockConnect = jest.fn(() => Promise.resolve({
        createChannel: jest.fn(() => Promise.resolve(mockChannel)),
        close: mockClose
    }));

    return {
        connect: mockConnect
    };
});

describe('UploadController', () => {
    describe('uploadFile', () => {
        test('deve retornar os objetos processados do serviço', () => {


            jest.spyOn(UploadService, 'processFile').mockReturnValueOnce(processedData);
            UploadController.uploadFile(req, res, next);
        });

        test('deve retornar um erro se nenhum arquivo for enviado', () => {
            const req = { file: null };
            const res = {
                status: jest.fn(() => res),
                json: jest.fn()
            };
            UploadController.uploadFile(req, res);
            expect(res.status).toHaveBeenCalledWith(400);
            expect(res.json).toHaveBeenCalledWith({ error: 'Nenhum arquivo enviado' });
        });
        test('deve chamar o próximo middleware se ocorrer um erro', async () => {
            const error = new Error('Erro de teste');
            UploadService.processFile.mockRejectedValue(error);

            await UploadController.uploadFile(req, res, next);

            expect(next).toHaveBeenCalledWith(error);
        });
    });
});
