const OrdersController = require('../../controllers/ordersController');
const OrderService = require('../../services/orderService');

// Mock de res
const mockResponse = () => {
    const res = {};
    res.json = jest.fn().mockReturnValue(res);
    res.status = jest.fn().mockReturnValue(res);
    return res;
};

// Mock de next
const mockNext = jest.fn();

// Mock de OrderService
jest.mock('../../services/orderService', () => ({
    getOrders: jest.fn(),
    getOrderByOrderId: jest.fn(),
    getOrdersByDate: jest.fn()
}));

describe('OrdersController', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    describe('getAll', () => {
        it('should return all orders', async () => {
            const req = {};
            const res = mockResponse();
            const orders = [{ id: 1, name: 'Order 1' }, { id: 2, name: 'Order 2' }];

            OrderService.getOrders.mockResolvedValue(orders);

            await OrdersController.getAll(req, res, mockNext);

            expect(OrderService.getOrders).toHaveBeenCalled();
            expect(res.json).toHaveBeenCalledWith(orders);
            expect(mockNext).not.toHaveBeenCalled();
        });

        it('should handle errors', async () => {
            const req = {};
            const res = mockResponse();
            const error = new Error('Internal Server Error');

            OrderService.getOrders.mockRejectedValue(error);

            await OrdersController.getAll(req, res, mockNext);

            expect(OrderService.getOrders).toHaveBeenCalled();
            expect(mockNext).toHaveBeenCalled();
        });
    });

});
