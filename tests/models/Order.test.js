const Order = require('../../models/order');

describe('Order Model', () => {
    test('should create an order instance', () => {
        const date = '2024-05-13'
        const order = Order.build({ orderId: '753', date: date });
        expect(order).toBeDefined();
        expect(order.orderId).toBe('753');
        expect(order.date).toBe(date);
    });
});
