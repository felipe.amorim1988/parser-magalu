const Product = require('../../models/product');

describe('Product Model', () => {
    test('should create a product instance', () => {
        const product = Product.build({ prodId: '3'});
        expect(product).toBeDefined();
        expect(product.prodId).toBe('3');
    });
});
