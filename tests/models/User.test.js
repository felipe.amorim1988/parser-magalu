const User = require('../../models/user');

describe('User Model', () => {
    test('should create a user instance', () => {
        const user = User.build({ userId: '123666', userName: 'Crybaby' });
        expect(user).toBeDefined();
        expect(user.userId).toBe('123666');
        expect(user.userName).toBe('Crybaby');
    });
});
