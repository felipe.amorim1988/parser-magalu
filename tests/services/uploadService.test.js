const UploadService = require('../../services/uploadService');
const fs = require('fs')
// Mock de amqplib
jest.mock('amqplib', () => {
    const mockSendToQueue = jest.fn();
    const mockClose = jest.fn();
    const mockChannel = {
        assertQueue: jest.fn(),
        sendToQueue: mockSendToQueue,
        close: mockClose
    };
    const mockConnect = jest.fn(() => Promise.resolve({
        createChannel: jest.fn(() => Promise.resolve(mockChannel)),
        close: mockClose
    }));

    return {
        connect: mockConnect
    };
});

describe('UploadService', () => {
    describe('processFile', () => {
        test('deve processar o arquivo corretamente', () => {
            const filePath = 'test.txt';
            const fileContent = `0000000070                              Palmer Prosacco00000007530000000003     1836.7420210308`;
            jest.spyOn(fs, 'readFileSync').mockReturnValueOnce(fileContent);
            const result = UploadService.processFile(filePath);
            const expected = [
                {
                    userId: '0000000070',
                    userName: 'Palmer Prosacco',
                    orders: [
                        {
                            orderId: '0000000753',
                            date: '20210308',
                            products: [
                                {
                                    prodId: '0000000003',
                                    value: 1836.74
                                }
                            ]
                        }
                    ]
                }
            ];
            expect(result).toEqual(expected);
        });
    });
    describe('sendMessageToQueue', () => {
        it('deve enviar mensagem para a fila', async () => {
            const mockMessage = 'mensagem de teste';
            const queueName = 'test-queue';

            await UploadService.sendMessageToQueue(queueName, mockMessage);

            expect(require('amqplib').connect).toHaveBeenCalled();
            expect(require('amqplib').connect.mock.calls[0][0]).toBe('amqp://localhost'); // Verifica se a conexão está sendo feita para o endereço correto
        });
    });
});
