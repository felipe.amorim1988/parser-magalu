const UserService = require('../../services/userService');
const User = require('../../models/user');

// Mock do modelo User
jest.mock('../../models/user', () => ({
    build: jest.fn(),
    create: jest.fn()
}));

describe('UserService', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    test('should create a user', async () => {
        User.create.mockResolvedValueOnce({ userId: '0000000070', userName: 'Palmer Prosacco' });
        const userData = { userId: '0000000070', userName: 'Palmer Prosacco' };
        const newUser = await UserService.createUser(userData);
        expect(newUser).toEqual({ userId: '0000000070', userName: 'Palmer Prosacco' });
    });

    // Testes semelhantes para os outros métodos do serviço (getUsers, getUserByUserId, etc.)
});
