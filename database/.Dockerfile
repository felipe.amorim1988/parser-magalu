# Use a imagem oficial do MySQL
FROM mysql:latest

# Defina variáveis de ambiente para configuração do MySQL
ENV MYSQL_ROOT_PASSWORD=root
ENV MYSQL_DATABASE=database
ENV MYSQL_USER=user
ENV MYSQL_PASSWORD=password

# Copie o script SQL para criar as tabelas (se necessário)
COPY ./init.sql /docker-entrypoint-initdb.d/

# Exponha a porta padrão do MySQL
EXPOSE 3306
