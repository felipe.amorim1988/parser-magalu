require('dotenv').config();

const amqp = require('amqplib');
const UploadService = require('./services/uploadService')
async function startWorker() {
    try {
        const connection = await amqp.connect('amqp://localhost');
        const channel = await connection.createChannel();
        await channel.assertQueue(process.env.QUEUE_NAME);

        console.log('Worker listening for messages...');

        channel.consume(process.env.QUEUE_NAME, async message => {
            if (message !== null) {
                const content = JSON.parse(message.content.toString());
                console.log('Received message:', content);
                await UploadService.importRows(content)
                channel.ack(message);
                console.log('proccess finished')
            }
        });
    } catch (error) {
        console.error('Error starting worker:', error);
    }
}

startWorker();
