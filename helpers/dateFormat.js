function format(dateString){
        // Extrair partes da string de data para formar a data JavaScript
        const year = dateString.substring(0, 4);
        const month = dateString.substring(4, 6);
        const day = dateString.substring(6, 8);

        // Criar um objeto de data JavaScript usando as partes extraídas
        const date = new Date(year, month - 1, day); // O mês é indexado em 0, então subtraia 1 do mês
        return date
}

module.exports = format