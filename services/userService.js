const User = require('../models/user');
const OrderService = require('./orderService')
class UserService {
    async createUser(userData) {
        return await User.create(userData);
    }

    async getUsers() {
        return await User.findAll();
    }

    async getUserByUserId(userId) {
        return await User.findOne({ where: { userId } });
    }

    async updateUserByUserId(userId, userData) {
        const user = await User.findOne({ where: { userId } });
        if (!user) return null;
        return await user.update(userData);
    }

    async deleteUserByUserId(userId) {
        const user = await User.findOne({ where: { userId } });
        if (!user) return null;
        return await user.destroy();
    }
    groupByUser(list) {
        const users = []
        list.forEach(item => {
            if (users.indexOf(u => u.userId == item.userId) == -1) {


                users.push({
                    userId: item.userId,
                    userName: item.userName,
                    orders: []
                })
            }

        })
        return OrderService.groupByOrder(users, list)
    }
}

module.exports = new UserService();
