const fs = require('fs');
require('dotenv').config();
const amqp = require('amqplib')
const { QUEUE_NAME, RABBIT_HOST } = process.env

const UserService = require('./userService')
const OrderService = require('./orderService')
const orderItemService = require('./orderItemService');
const productService = require('./productService');
const userService = require('./userService');
class UploadService {
    static processFile(filePath) {
        try {
            const data = fs.readFileSync(filePath, 'utf8');
            const lines = data.split('\n');
            const objects = [];

            lines.forEach(line => {
                const userId = line.substring(0, 10).trim();
                const userName = line.substring(40, 55);
                const orderId = line.substring(55, 65).trim();
                const prodId = line.substring(65, 75).trim();
                const value = parseFloat(line.substring(80, 87).trim());
                const date = line.substring(87).trim();

                objects.push({
                    userId,
                    userName,
                    orderId,
                    prodId,
                    value,
                    date
                });
            });
            const response = userService.groupByUser(objects)
            //this.importRows(response)
            this.sendMessageToQueue(QUEUE_NAME, response);
            return response
        } catch (error) {
            throw error
        }
    }
    static async sendMessageToQueue(queue, message) {
        try {
            const connection = await amqp.connect(`amqp://${RABBIT_HOST}`);
            const channel = await connection.createChannel();
            await channel.assertQueue(queue);
            await channel.sendToQueue(queue, Buffer.from(JSON.stringify(message)));
            console.log(`Message sent to queue "${queue}"`);
            await channel.close();
            await connection.close();
        } catch (error) {
            console.error('Error sending message to queue:', error);
        }
    }

    static async importRows(list) {
        for (let user of list) {
            const u = await UserService.getUserByUserId(user.userId)
            if (!isNaN(parseInt(user.userId)))
                if (u == null)
                    await UserService.createUser({
                        id: null,
                        userId: user.userId,
                        userName: user.userName
                    })

            for (let order of user.orders) {
                const o = await OrderService.getOrderByOrderId(order.orderId)
                if (!isNaN(parseInt(order.orderId)))
                    if (o == null)
                        await OrderService.createOrder({
                            id: null,
                            orderId: order.orderId,
                            date: order.date
                        })
                for (let prod of order.products) {
                    const p = await productService.getProductByProdId(prod.prodId)
                    if (!isNaN(parseInt(prod.prodId))) {
                        if (p == null)
                            await productService.createProduct({
                                id: null,
                                prodId: prod.prodId,
                            })
                        const oi = await orderItemService.getOrderItemByOrderId(order.orderId, prod.prodId)
                        if (oi == null) {
                            await orderItemService.createOrderItem({
                                id: null,
                                orderId: order.orderId,
                                prodId: prod.prodId,
                                value: prod.value
                            })
                        }
                    }
                }
            }
        }
    }
}

module.exports = UploadService;
