const Order = require('../models/order');
const OrderItem = require('../models/orderItem');
const Product = require('../models/product');
const { Op } = require('sequelize');

const ProductService = require('./productService')
const dateFormat = require('../helpers/dateFormat')
class OrderService {
    async createOrder(orderData) {
        orderData.date = dateFormat(orderData.date);
        return await Order.create(orderData);
    }

    async getOrders() {
        return await Order.findAll({
            include: [{
                model: OrderItem,
                include: [{
                    model: Product,
                    attributes: ['prodId'] 
                }],
                attributes: ['value'] 
            }]
        });
    }

    async getOrdersByDate(startDate, endDate) {
        const orders = await Order.findAll({
            include: [{
                model: OrderItem,
                include: [{
                    model: Product,
                    attributes: ['prodId'] 
                }],
                attributes: ['value'] 
            }],
            where: {
                date: {
                    [Op.between]: [dateFormat(startDate), dateFormat(endDate)]
                }
            }
        });

        return orders;
    }

    async getOrderByOrderId(orderId) {
        const order = await Order.findOne({ where: { orderId } });
        if(order != null){
            const orderItems = await OrderItem.findAll({
                where: {
                    orderId: orderId
                },
                include: [{
                    model: Product,
                    required: true,
                    attributes: ['prodId']
                }],
                raw: true
            });        
            const products = orderItems.map(orderItem => ({
                prodId: orderItem.prodId,
                value: orderItem.value
            })); 
            return {
                orderId: order.orderId,
                date: order.date,
                products: products
            };
        }
       return order
    }
    

    async updateOrderByOrderId(orderId, orderData) {
        const order = await Order.findOne({ where: { orderId } });
        if (!order) return null;
        return await order.update(orderData);
    }

    async deleteOrderByOrderId(orderId) {
        const order = await Order.findOne({ where: { orderId } });
        if (!order) return null;
        return await order.destroy();
    }

    groupByOrder(users, list) {
        list.forEach(item => {
            users.forEach(user => {
                if (item.userId == user.userId
                    &&
                    user.orders
                        .indexOf(order =>
                            item.orderId
                            ==
                            order.orderId) == -1) {
                    user.orders.push({
                        orderId: item.orderId,
                        total: item.total,
                        date: item.date,
                        products: []
                    })
                }

            })
        })
        users.forEach(user => {
            user.orders = this.removeDuplicatesOrders(user.orders)
        })
        return ProductService.groupByProduct(users,list)
    }

    removeDuplicatesOrders(orders) {
        const uniqueOrders = orders.reduce((acc, current) => {
            const orderId = current.orderId;
            const existingOrder = acc.find(order => order.orderId === orderId);
            if (!existingOrder) {
                acc.push(current);
            }
            return acc;
        }, []);

        return uniqueOrders;
    }
}

module.exports = new OrderService();
