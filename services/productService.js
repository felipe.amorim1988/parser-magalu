const Product = require('../models/product');

class ProductService {
    async createProduct(productData) {
        return await Product.create(productData);
    }

    async getProducts() {
        return await Product.findAll();
    }

    async getProductByProdId(prodId) {
        return await Product.findOne({ where: { prodId } });
    }

    async updateProductByProdId(prodId, productData) {
        const product = await Product.findOne({ where: { prodId } });
        if (!product) return null;
        return await product.update(productData);
    }

    async deleteProductByProdId(prodId) {
        const product = await Product.findOne({ where: { prodId } });
        if (!product) return null;
        return await product.destroy();
    }
    groupByProduct(users, list) {
        list.forEach(item => {
            users.forEach(user => {
                user.orders.forEach(order => {
                    if (item.userId == user.userId
                        &&
                        item.orderId == order.orderId
                    )
                        order.products.push({
                            prodId: item.prodId,
                            value: item.value
                        })
                })
            })
        })
        users.forEach(user =>
            user.orders.forEach(order =>
                order.products =
                this.removeDuplicatesProducts(order.products)
            )
        )
        return users
    }
    removeDuplicatesProducts(products) {
        const uniqueProducts = products.reduce((acc, current) => {
            const prodId = current.prodId;
            const existingProd = acc.find(prod => prod.prodId === prodId);
            if (!existingProd) {
                acc.push(current);
            }
            return acc;
        }, []);

        return uniqueProducts;
    }
}

module.exports = new ProductService();
