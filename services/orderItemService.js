const OrderItem = require('../models/orderItem');

class OrderItemService {
    async createOrderItem(orderData) {
        return await OrderItem.create(orderData);
    }

    async getOrdersItem() {
        return await OrderItem.findAll();
    }

    async getOrderItemByOrderId(orderId,prodId) {
        return await OrderItem.findOne({ where: { orderId, prodId } });
    }

    async updateOrderItemByOrderId(orderId, orderData) {
        const order = await OrderItem.findOne({ where: { orderId } });
        if (!order) return null;
        return await order.update(orderData);
    }

    async deleteOrderItemByOrderId(orderId) {
        const order = await OrderItem.findOne({ where: { orderId } });
        if (!order) return null;
        return await order.destroy();
    }
}

module.exports = new OrderItemService();
