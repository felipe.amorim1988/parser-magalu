const express =require('express')
const OrdersController =require('../controllers/ordersController')
const router = express.Router();
router.get('/orders',OrdersController.getAll)
router.get('/orders/:orderId',OrdersController.getById)
router.get('/orders/:startDate/:endDate', OrdersController.getByDate);


module.exports = router;
