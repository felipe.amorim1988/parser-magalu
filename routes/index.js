const uploadRoutes = require('./uploadRoutes');
const ordersRoutes = require('./ordersRoutes');
class Routes{
    static build(app){
        app.use('/', uploadRoutes);
        app.use('/', ordersRoutes);
    }
}

module.exports = Routes