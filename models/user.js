const Sequelize = require('sequelize');
const sequelize = require('../database/sequelize');

const User = sequelize.define('user', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    userId: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    userName: {
        type: Sequelize.STRING,
        allowNull: false
    }
}, {
    timestamps: false // Remove os campos createdAt e updatedAt
});

module.exports = User;
