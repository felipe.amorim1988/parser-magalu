const {Model,DataTypes} = require('sequelize');
const sequelize = require('../database/sequelize');
const Order = require('./order');
const Product = require('./product');
class OrderItem extends Model{}
OrderItem.init({
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    orderId: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    prodId: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    value: {
        type: DataTypes.FLOAT,
        allowNull: false
    }
}, {sequelize,
    modelName: 'orderItem',
    timestamps: false // Remove os campos createdAt e updatedAt
});
Order.hasMany(OrderItem, { foreignKey: 'orderId', sourceKey:'orderId' }); 

OrderItem.belongsTo(Order, {foreignKey: 'orderId', targetKey:'orderId'})
OrderItem.belongsTo(Product, {foreignKey: 'prodId', targetKey:'prodId'})


module.exports = OrderItem;
