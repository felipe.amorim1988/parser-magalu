const sequelize = require('../database/sequelize');
const { DataTypes, Model } = require('sequelize');

const Order = sequelize.define('order', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    orderId: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    date: {
        type: DataTypes.DATEONLY,
        allowNull: false
    }
}, {
    timestamps: false // Remove os campos createdAt e updatedAt
});
//Order.hasMany(OrderItem, { foreignKey: 'orderId' }); 
module.exports = Order;
