const { DataTypes, Model } = require('sequelize');
const sequelize = require('../database/sequelize');
class Product extends Model{}
Product.init( {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    prodId: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
}, {sequelize,
    modelName: 'product',
    timestamps: false // Remove os campos createdAt e updatedAt
});

module.exports = Product;
